# Auto Config API

Annotation based API for mod configs, with Cloth Config GUI integration.
For usage instructions, see the [wiki].

## Building from source

```bash
git clone https://gitlab.com/sargunv-mc-mods/auto-config.git
cd auto-config
./gradlew build
# On Windows, use "gradlew.bat" instead of "gradlew"
```

[wiki]: https://gitlab.com/sargunv-mc-mods/auto-config/wikis/home